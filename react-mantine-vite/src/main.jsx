import React from 'react'
import ReactDOM from 'react-dom'
import { 
  BrowserRouter, Routes, Route,
} from 'react-router-dom'

import './index.css'

import App from './App'
import Login from './routes/Login'

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter >
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="/login" element={<Login />} />
       
        
        <Route 
          path="*" 
          element={
            <main>
              <p>Nothing Found ! </p>
            </main>
          } 
        />        
      </Routes>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
)