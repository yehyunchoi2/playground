import { useState } from 'react'
import { Link } from 'react-router-dom'
import { Anchor } from '@mantine/core'

import './App.css'

function App() {
  

  return (
    <div className="app">
      <Anchor component={Link} to="/login" >
        <button>Go to Login</button>
      </Anchor>
    </div>
  )
}

export default App