import React from 'react'

import { Link } from 'react-router-dom'
import { Anchor } from '@mantine/core'

function Login() {
  return (
    <main>
        <Anchor component={Link} to="/" > 
          <button>Go back</button>
        </Anchor>
        <h1>Login page</h1>
    </main>
  )
}

export default Login